###########################################################
#### Download from Uniprot Reference Proteome #############
#### Only Choose the Eukaroyota ###########################
#### ftp://ftp.uniprot.org/pub/databases/uniprot/ #########
#### current_release/knowledgebase/reference_proteomes/ ###
###########################################################

import re 
import os 

#### Unzip the .gz file 
#### Combine all fasta file to Single One fasta file
out_file = 'AllRefPep.fasta'
out = open(out_file, 'w')
for root, subdir_list, file_list in os.walk('/data/database/UniprotReferenceProteome/Eukaryota/'):
    for each_file in file_list:
        file_abs_path = root + each_file
        if each_file.endswith('fasta.gz'):
            unzip_cmd = 'gunzip -k  ' + file_abs_path
            # os.system(unzip_cmd)
        
        if each_file.endswith('fasta'):
            f = open(file_abs_path, 'r')
            for line in f:
                out.write(line)
            f.close()
out.close()
