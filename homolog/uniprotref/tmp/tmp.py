#! /usr/bin/python3

import re
import os

###########################################################
#### A Quick filtering based on identity ##################
###########################################################


for root, subdir_list, file_list in os.walk('/data/database/cancerSNP/benchmark/humdiv/MSA/Result/'):
    for each_file in file_list:
        if each_file.endswith('Iden30.fasta.entropy'):
            file_abs_path = root + each_file
            f = open(file_abs_path, 'r')
            for line in f:
                line = line.strip()
                print(line)
            f.close()