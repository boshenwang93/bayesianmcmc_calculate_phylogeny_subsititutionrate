#! /usr/bin/python3 

import xml.etree.ElementTree as ET 
import os
import re

###########################################################
#### Parse BLAST XML out, Generate Pairwise Fasta #########
#### Between the query sequence and candidate sequence ####
###########################################################


#######################################
#### Parse BLAST XML Out ##############
#######################################
def ChooseHits(BLAST_XML_Output_file):

    ## initialize dictionary(uniprot=> list hits) 
    list_hits = []
    uniprot_id = ''

    ## read the XML file into a ET parser
    tree = ET.parse(BLAST_XML_Output_file)
    root = tree.getroot()

    ## find hits 
    for first_layer in root:    
        if 'BlastOutput_query-def' == first_layer.tag:
            query_info = first_layer.text
            uniprot_id += query_info.upper()
            uniprot_id = uniprot_id.strip().upper()

        ##find the iterations segments
        elif 'BlastOutput_iterations' == first_layer.tag:
            for second_layer in first_layer:
                if 'Iteration' == second_layer.tag:
                    for third_layer in second_layer:
                        if 'Iteration_hits' == third_layer.tag:
                            for fourth_layer in third_layer:
                                if 'Hit' == fourth_layer.tag:
                                    homolog_candidate = ''
                                    for fifth_layer in fourth_layer:
                                        if 'Hit_def' == fifth_layer.tag:
                                            homolog_candidate += fifth_layer.text
                                    list_hits.append(homolog_candidate)

    return uniprot_id, list_hits


#######################################
#### Obtain Sequence Info #############
#######################################
def Retrieve_Uniprot_Seq(uniprot_fasta_file_path):
    seq_content = ''
    with open(uniprot_fasta_file_path, 'r') as f:
        seq_content += f.read()
    f.close()
    return seq_content

#######################################
#### Create Raw Pairwise .fa ##########
#######################################
def CreateRawPairFasta(uniprotID1_abs_path, uniprotID2_abs_path, out_file_path):

    seq1 = Retrieve_Uniprot_Seq(uniprot_fasta_file_path = uniprotID1_abs_path)
    seq2 = Retrieve_Uniprot_Seq(uniprot_fasta_file_path = uniprotID2_abs_path)

    out_content = seq1 + "\n" + seq2

    out_f = open(out_file_path, 'w')
    out_f.write(out_content)
    out_f.close()

    return 0 


def main(): 
    ## dic for uniprot => list best hits
    dic_uniprot_besthits = {}

    ##BLAST XML output path
    BLAST_XML_dir = '/data/database/UniprotReferenceProteome/BLASThits/'
    
    ## uniprot fasta file path 
    uniprot_fasta_dir = '/data/database/UniprotReferenceProteome/fasta/'

    ## BLAST all fasta in folder 
    for root, subdir_list, file_list in os.walk(BLAST_XML_dir):
        for single_file in file_list:
            file_asb_path = BLAST_XML_dir + single_file
            if single_file.endswith('.XML'):
                uniprot, list_hits = ChooseHits(BLAST_XML_Output_file = file_asb_path)

                ## print out the Uniprot => Homolog Hits
                if list_hits != []:
                    for each_hit in list_hits:
                        HumSeq_path = '/home/bwang/project/binarysnp/data/polyphen2/Div/fasta/' + uniprot + '.fasta'
                        hit_path = uniprot_fasta_dir + each_hit + '.fasta'

                        new_file_path =  '/data/database/UniprotReferenceProteome/RawPair/' +\
                                         uniprot + "_" + each_hit + '.fasta'
                        
                        if uniprot != each_hit:
                            try:
                                CreateRawPairFasta( uniprotID1_abs_path = HumSeq_path,
                                                    uniprotID2_abs_path = hit_path,
                                                    out_file_path = new_file_path)
                            except:
                                print(new_file_path)
    return 0 

if __name__ == '__main__':
    main()