import re 
import os 

###########################################################
#### A Quick filtering based on identity ##################
###########################################################



#####################################
list_uniprotHum = []

f2 = open('/home/bwang/calculation/full/MSA/identity.csv', 'r')
for line in f2:
    uniprot_Human = re.split(',', line)[0]
    uniprot_Human = re.split('>', uniprot_Human)[1]
    if uniprot_Human not in list_uniprotHum:
        list_uniprotHum.append(uniprot_Human)
f2.close()


for each_uniprotHum in list_uniprotHum:
    assigned_new_dir = '/home/bwang/calculation/full/MSA/rawgroup/'  + each_uniprotHum + '/'
    mkdir_cmd = 'mkdir ' + assigned_new_dir
    os.system(mkdir_cmd)
    
    source_fasta = '/home/bwang/calculation/full/sequence/' + each_uniprotHum + '.fasta'
    dest_fasta = assigned_new_dir 

    cp1_cmd = 'cp ' + source_fasta + '  ' + dest_fasta + "\n"
    os.system(cp1_cmd)

    f_in = open('/home/bwang/calculation/full/MSA/identity.csv', 'r')
    for line in f_in:
        uniprot_Human = re.split(',', line)[0]
        uniprot_Human = re.split('>', uniprot_Human)[1]

        if each_uniprotHum == uniprot_Human:
            uniprot_nonHum = re.split(',', line)[1]
            uniprot_nonHum = re.split('>', uniprot_nonHum)[1]

            iden_seq1 = re.split(',', line)[2].strip()
            iden_seq1 = float(iden_seq1)
            iden_seq2 = re.split(',', line)[3].strip()
            iden_seq2 = float(iden_seq2)

            source_fasta = '/home/bwang/calculation/single/' + uniprot_nonHum + '.fasta'
            dest_fasta = assigned_new_dir 
            
            if iden_seq1 >= 0.5:
                if iden_seq2 >= 0.5:
                    cp2_cmd = 'cp ' + source_fasta + '  ' + dest_fasta + "\n"
                    os.system(cp2_cmd)

    f_in.close()