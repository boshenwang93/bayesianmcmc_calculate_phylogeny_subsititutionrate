#! /usr/bin/python3 

import re 
import os 

#### For the fasta file containing multiple entries 
#### Seperate each single one to a single file named as Uniprot Name 

def SeperateFasta(Combinated_Fasta_File, out_fasta_dir):

    ## read whole content into a string variable 
    all_context = ''

    f = open(Combinated_Fasta_File, 'r')
    for line in f:
        line = line.strip()
        if line.startswith(">"):
            uniprot = re.split("\|", line)[1].strip().upper()
            header = ">" + uniprot + "\n"
            all_context += header 
        else:
            all_context += line
    f.close()
    
    ## split each sequence context 
    list_slices = re.split('>', all_context)
    
    ## iterate each sequence 
    for i in range(0, len(list_slices)):
        each_sequence = list_slices[-1]
        list_slices.pop()

        sequence_with_header = '>' + each_sequence
        list_lines_seq = re.split("\n", sequence_with_header)

        ## to obtain header and actual sequence(Remove \n)
        uniprot_header = ''
        seq_context = ''

        for eachline in list_lines_seq:
            if eachline.startswith('>') == 0 :
                if eachline != '':
                    seq_context += eachline
            else:
                uniprot_header += eachline

        ## get uniprot id 
        uniprot_id = re.split('>', uniprot_header)[1]

        # new file name & path 
        new_file_name = uniprot_id + '.fasta'
        new_file_path = out_fasta_dir + new_file_name

        # file content 
        new_content = '>' + uniprot_id + "\n" + seq_context
            
        # write to new file 
        out = open(new_file_path, 'w')
        out.write(new_content)
        out.close()
    
    return 0

if __name__ == '__main__':

    for root, subdir_list, file_list in os.walk('/data/database/UniprotReferenceProteome/Eukaryota/'):
        for each_file in file_list:
            if each_file.endswith('.fasta'):
                file_abs_path = '/data/database/UniprotReferenceProteome/Eukaryota/' + each_file
                
                # unzip_cmd = 'gzip -d '  + file_abs_path
                # os.system(unzip_cmd)

                ## Get the 
                SeperateFasta(Combinated_Fasta_File =  file_abs_path ,
                      out_fasta_dir = '/data/database/UniprotReferenceProteome/fasta/')