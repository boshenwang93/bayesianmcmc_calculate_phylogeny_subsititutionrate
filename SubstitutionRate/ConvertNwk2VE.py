#! /usr/bin/python3

import re
import numpy 


###########################################################
#### Convert NWK format to Node, Edge #####################
#### Sep 2019, By B.W. ####################################
###########################################################


#### Check the Correctness of Bifuracting Tree Topology
#### Three types: Unroot, Root, Nonstandard
def Check_Correctness_Topology(NWK_file):
    
    ## read the raw nwk file text
    f = open(NWK_file, 'r')
    tree_in_line = f.read()
    f.close()
    
    tree_nwk_text = re.split(';', tree_in_line)[0]

    ## Obtain the number of total nodes/vertex (internal+external) 
    count_total_nodes = 0
    for i in range(0,len(tree_nwk_text)):
        if tree_nwk_text[i] == ':':
            count_total_nodes += 1
    
    count_external_nodes = 0
    for i in range(0,len(tree_nwk_text)):
        if tree_nwk_text[i] == '_':
            count_external_nodes += 1
    
    ## Checking the node Correct
    Tree_classification_info = ''
    if count_total_nodes == 2 * count_external_nodes - 2:
        ## Pass the unRooted tree
        Tree_classification_info += 'Unrooted'
    elif count_total_nodes == 2 * count_external_nodes -1:
        ## pass the rooted tree
        Tree_classification_info += 'Rooted'
    else:
        ## not the standard bifuracting tree
        Tree_classification_info += 'Non-Standard Format' 
    
    return Tree_classification_info


####@@@@!!!! TimeofTree Gives Out ROOT Tree!!!! 
#### Input the Bifuracting & 
####           Rooted & 
####           Divergence-time Tree !!!!!!!
def ConvertNwk2VE_root(NWK_file):

    ## read the raw nwk file text
    f = open(NWK_file, 'r')
    tree_in_line = f.read()
    f.close()
    
    tree_nwk_text = re.split(';', tree_in_line)[0]

    ## Obtain the number of total nodes/vertex (internal/external/total) 
    count_total_nodes = 0
    for i in range(0,len(tree_nwk_text)):
        if tree_nwk_text[i] == ':':
            count_total_nodes += 1
    ## Adjust the root node
    count_total_nodes += 1 
    
    count_external_nodes = 0
    for i in range(0,len(tree_nwk_text)):
        if tree_nwk_text[i] == '_':
            count_external_nodes += 1
    
    count_internal_nodes = count_total_nodes - count_external_nodes
    
    ## initialize the Hash tableS
    ## !! USE BFS layer by layer 
    ## External Node => Species Name and Vice Versa
    ## Node Pair => Up-layer Interal Node
    ## Edge => Distance
    ## Up-Node Low-Node Pair => Edge
    ## Node => Layer 
    dic_ExNode_Species = {}
    dic_Species_Exnode = {}
    dic_NodePair_UpNode = {}
    dic_Edge_distance = {}
    dic_uplowNodePair_Edge = {}
    dic_Node_Layer = {}
    
    ## Initialize the Index for rolling update
    ## Node is range from V1 to V(2*N - 1)
    ## Edge is range from E1 to E(2*N - 2)
    index_Node = 0
    index_Edge = 0

    ## To Obtain the Hash table for Extenal Node => Species 
    list_speciesWdist = re.findall("\w+\:\d+.\d+", 
                                  tree_nwk_text, flags= re.IGNORECASE)
    for i in range(0, len(list_speciesWdist)):
        species_name = re.split("\:", list_speciesWdist[i])[0].strip()
        ## Assign the External Node => Species name
        external_index = 'V' + str(i+1)
        dic_ExNode_Species [external_index] = species_name
        dic_Species_Exnode [species_name] = external_index
        ## Rolling update the index of Node
        index_Node += 1 
    ## dic_ExNode_Species and Vice Versas Hash Table is DONE!

    ## To obtain the number of layers
    ## The maximum layer should be equal to ==  #Species/ExNode
    # Remove the fake node number by TreeOfLife
    tree_nwk_text = re.sub("\'\d+\'", '', tree_nwk_text)

    count_layers = 0
    tree_text_cp = tree_nwk_text
    for i in range(1, count_external_nodes):
        tree_text_cp = re.sub("\(\w+\:\d+.\d+,\w+\:\d+.\d+\)", "Sp1_Sp2",
                                  tree_text_cp, flags= re.IGNORECASE)
        # Once reach out the Outside, STOP replacement
        if tree_text_cp == 'Sp1_Sp2':
            count_layers += i
            break
    ## Adjust the Root Node
    count_layers += 1 
    ## Obtaining the total number of layers DONE!!

    ## Convert the Raw tree Text into Node index
    tree_text_V = tree_nwk_text
    for each_speciesWdist in list_speciesWdist:
        species_name = re.split("\:", each_speciesWdist)[0].strip()
        node_index = dic_Species_Exnode[species_name]
        tree_text_V = re.sub( species_name, node_index,
                                tree_text_V  , flags= re.IGNORECASE)
    # Now we obtain the tree_text in V mode
    
    ## BFS To trim the Node layer by layer
    for i in range(0, count_layers):
        # print(tree_text_V)
        ## initialize the current list for a compact node pair 
        list_nodepair_currentLayerI = []
        list_nodepair_currentLayerI = re.findall("\(\w+\:\d+.\d+,\w+\:\d+.\d+\)", 
                                          tree_text_V, flags= re.IGNORECASE)

        ## To generate UpNode layer by layer
        for j in  range(0, len(list_nodepair_currentLayerI) ):
            ## Obtain the current NodePair 
            each_nodepair = list_nodepair_currentLayerI[j]

            part1 = re.split(',', each_nodepair)[0]
            part1_node = re.split("\:", part1)[0]
            part1_node = part1_node[1:]
            part1_dist = re.split("\:", part1)[1]

            part2 = re.split(',', each_nodepair)[1]
            part2_node = re.split("\:", part2)[0]
            part2_dist = re.split("\:", part2)[1]
            part2_dist = part2_dist[0:-2]

            ## Push the Hash NodeIndex => Layer
            Layer_index = 'Layer' + str(i+1)
            dic_Node_Layer [part1_node] = Layer_index
            dic_Node_Layer [part2_node] = Layer_index
            
            # For checking 
            # print(each_nodepair, ',', 
            #       part1_node, ',', part1_dist,
            #       part2_node, ',', part2_dist)

            ## Assign the Up-Layer Node Index
            index_Node += 1
            update_Node_index = 'V' + str(index_Node)

            ## Push the Hash table NodePair => UpNode
            NodePair = part1_node + '_' + part2_node
            dic_NodePair_UpNode [NodePair] = update_Node_index
            
            ## Assign the Edge, Edge => Dist, uplowNodePair => Edge
            index_Edge += 1 
            edge1 = "E" + str(index_Edge)
            uplowNodePair1 = part1_node + '_' + update_Node_index
            index_Edge += 1 
            edge2 = "E" + str(index_Edge)
            uplowNodePair2 = part2_node + '_' + update_Node_index

            dic_Edge_distance [edge1] = part1_dist
            dic_Edge_distance [edge2] = part2_dist

            dic_uplowNodePair_Edge [uplowNodePair1] = edge1
            dic_uplowNodePair_Edge [uplowNodePair2] = edge2

            ##!!!!!!! To trim to a simplified text literally
            list_tmp_threePCs = re.split(each_nodepair, tree_text_V)

            prefix = list_tmp_threePCs[0]
            postfix = list_tmp_threePCs[2]
            count_pre = len(prefix)
            count_post = len(postfix)

            tree_text_V = prefix[0:count_pre-1] +\
                          update_Node_index +\
                          postfix[1:count_post]
            ## Update a new layer NWK text DONE!
        ## Adjust the Root Node
        if i == count_layers - 1:
            root_node_index = "V" + str(2*len(list_speciesWdist) - 1)
            last_layer_index = "Layer" + str(i+1)
            dic_Node_Layer [root_node_index] = last_layer_index
        # print("**********************")

    print(dic_ExNode_Species, "\n",
          dic_Species_Exnode , "\n",
          dic_NodePair_UpNode , "\n",
          dic_Edge_distance , "\n",
          dic_uplowNodePair_Edge , "\n",
          dic_Node_Layer)

    return 0

ConvertNwk2VE_root(NWK_file = './SampleNwkFig/5Species.nwk')

