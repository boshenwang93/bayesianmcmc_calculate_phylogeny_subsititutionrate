import re

list_chosen_species = []
species_F = open('SubsetHighSimilarSpeciesSample', 'r')
for line in species_F:
    up_abbv = line.strip()
    list_chosen_species.append(up_abbv)
species_F.close()

dic_upabbv_taxonNo = {}
dic_upabbv_fullName = {} 

f = open('CrossRefSpeciesID', 'r')
for line in f:
    if re.search(":", line):
        up_abbv = line[0:5]
        up_abbv = up_abbv.strip()
        
        taxonomyNO = line[8:15]
        taxonomyNO = taxonomyNO.strip()
        dic_upabbv_taxonNo [up_abbv] = taxonomyNO

        scientificName = re.split("N=", line)[-1].strip()
        dic_upabbv_fullName [up_abbv] = scientificName
f.close()

for each_upabbv in list_chosen_species:
    out_entry = dic_upabbv_fullName[each_upabbv] + ',' +\
                dic_upabbv_taxonNo[each_upabbv]
    print(out_entry)
