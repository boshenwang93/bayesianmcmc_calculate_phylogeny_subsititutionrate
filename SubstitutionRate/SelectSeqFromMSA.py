import re 

dic_seqID_speciesID = {} 
hash_F = open('/data/database/uniprot_Ref_Jun2019/OtherEukaryota/SeqID_SpeciesID', 'r')
for line in hash_F:
    line = line.strip()
    seqID = re.split(',', line)[0]
    speciesID = re.split(',', line)[1]

    dic_seqID_speciesID [seqID] = speciesID
hash_F.close()

dic_speciesID_scienticName = {} 
tmp_f = open('Top25Species', 'r')
for line in tmp_f:
    line = line.strip()
    speciesID = re.split(',', line)[1]
    scientificName = re.split(',', line)[0]

    dic_speciesID_scienticName[speciesID] = scientificName
tmp_f.close()

dic_seqPair_identity1 = {}
dic_seqPair_identity2 = {}
identity_F = open('/data/database/uniprot_Ref_Jun2019/homologs/orthologs/MSA/humanUP_ortholog_Iden30.csv', 'r')
for line in identity_F:
    line = line.strip()
    list_linepieces = re.split(',', line)

    seq1_header = list_linepieces[0]
    seq2_header = list_linepieces[1]
    identity_seq1 = float(list_linepieces[2])
    identity_seq2 = float(list_linepieces[3])

    seq1_uniprotID = re.split("\|", seq1_header)[1].strip()
    seq2_uniprotID = re.split("\|", seq2_header)[1].strip()
    
    seq_pair = seq1_uniprotID + '_' + seq2_uniprotID
    dic_seqPair_identity1 [seq_pair] = identity_seq1
    dic_seqPair_identity2 [seq_pair] = identity_seq2
identity_F.close()


def ReplaceSeqHeader(fasta_file_PATH):

    raw_content = ''
    with open(fasta_file_PATH, 'r') as original_F:
        raw_content += original_F.read()
    original_F.close()
    
    list_sequences = re.split('>', raw_content)
    for each_seq in list_sequences:
        each_seq = '>' + each_seq
        
        ## Obtain  the header and only_seq_info
        old_header = ''
        only_seq = ''
        list_lines = re.split("\n", each_seq) 
        for each_line in list_lines:
            if each_line.startswith('>'):
                old_header += each_line
            else:
                only_seq += each_line
        ## Done

        try:
            uniprotID = re.split("\|", old_header)[1].upper().strip()
            taxonomyID = dic_seqID_speciesID[uniprotID]
            scientificName = dic_speciesID_scienticName[taxonomyID]
            new_header = '>' + uniprotID + '_' + scientificName 
            print(new_header)
            print(only_seq)
        except:
            pass

    return 0

ReplaceSeqHeader(fasta_file_PATH = 'P21673.fasta')