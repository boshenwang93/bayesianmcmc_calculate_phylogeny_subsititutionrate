import numpy
import numpy.linalg as linalg
from numpy.linalg import matrix_power

###########################################################
#### Bayesian MCMC to estimate substitution rate ##########
#### Written by B.W. ######################################
#### Step 1. select region, obtain diagonal M for 20 a.a. #
#### Step 2. Decompose instaneous matrix Q ################


# Hash table for amino acid => index in matrix
dic_amino_index = {
    "A": 0,
    "R": 1,
    "N": 2,
    "D": 3,
    "C": 4,
    "Q": 5,
    "E": 6,
    "G": 7,
    "H": 8,
    "I": 9,
    "L": 10,
    "K": 11,
    "M": 12,
    "F": 13,
    "P": 14,
    "S": 15,
    "T": 16,
    "W": 17,
    "Y": 18,
    "V": 19
}

dic_index_amino = {
    0: "A",
    1: "R",
    2: "N",
    3: "D",
    4: "C",
    5: "Q",
    6: "E",
    7: "G",
    8: "H",
    9: "I",
    10: "L",
    11: "K",
    12: "M",
    13: "F",
    14: "P",
    15: "S",
    16: "T",
    17: "W",
    18: "Y",
    19: "V"
}


# list for Amino Acid One Letter
list_amino_acid = [
    "A",
    "R",
    "N",
    "D",
    "C",
    "Q",
    "E",
    "G",
    "H",
    "I",
    "L",
    "K",
    "M",
    "F",
    "P",
    "S",
    "T",
    "W",
    "Y",
    "V"
]

# initialize the identical matrix I
I = numpy.full((20, 20), 1)
for i in range(0, 20):
    for j in range(0, 20):
        if i != j:
            I[i][j] = 0
# I is done


#######################################
#### Approximate P(t) = e^Qt    #######
#### utilize N in (0-1000)  ###########
#### Input: rate Q, time intv #########
#######################################

def Numerical_eQt(givenQ, time_interval):

    Pt = numpy.full((20, 20), 0.0)

    for N in range(0, 100):
        constant = numpy.power(time_interval, N) / numpy.math.factorial(N)
        tmpP = constant * matrix_power(Q, N)
        Pt += tmpP

    # print(Pt)
    return Pt


#######################################
#### Initilize Q matrix P = e^Qt   ####
#### Q infinitesimal matrix ###########
#######################################

#### Option 1, random rate to initialize Q
# Q = numpy.full((20, 20), 0)
# for i in range(0, 20):
#     for j in range(0, 20):
#         if j > i:
#             Q[i][j] = numpy.random.choice(
#                 [1.0, 5.0, 8.0, 3.0, 2.0])
#             # Q [i][j] = 2
# # sysmetric
# for i in range(0, 20):
#     for j in range(0, 20):
#         if j < i:
#             Q[i][j] = Q[j][i]
# for i in range(0, 20):
#     sum_other_rate = 0
#     for j in range(0, 20):
#         if i != j:
#             sum_other_rate += Q[i][j]
#     # Done
#     Q[i][i] = -1 * sum_other_rate


#### Option 2 Uniform Rate to initialize Q
Q = numpy.full((20, 20), 0)
for i in range(0, 20):
    for j in range(0, 20):
        if j != i:
            Q[i][j] = 2
##
for i in range(0, 20):
    sum_other_rate = 0
    for j in range(0, 20):
        if i != j:
            sum_other_rate += Q[i][j]
    # Done
    Q[i][i] = -1 * sum_other_rate
## Q is generated


#######################################
#### N_species by M_indepSites in MSA##
#######################################

def Obtain_MSA_2dArray(aligned_fasta_file):

    # initalize hash table for fake_site_index Residue => Count
    dic_fakeindexRes_count = {}
    count_homologs = 0

    # Pass the file content to a string
    file_content = ''
    f = open(aligned_fasta_file, 'r')
    for line in f:
        file_content += line
    f.close()
    file_content = file_content.upper()

    # Split the file content to each sequence
    list_seqs = file_content.split('>')
    count_homologs += len(list_seqs) - 1

    # Iterate every seq to obtain actual aligned sequence list
    list_actual_aligned_sequence = []

    for j in range(1, len(list_seqs)):
        each_seq_w_header = list_seqs[j]
        list_lines = each_seq_w_header.split("\n")
        # sequence ID header
        header = list_lines[0]
        # actual sequence
        actual_sequence = ''
        for i in range(1, len(list_lines)):
            actual_sequence += list_lines[i]
        list_actual_aligned_sequence.append(actual_sequence)

    fake_length = len(list_actual_aligned_sequence[1])

    # Iterate every site to count the AminoAcid frequency
    for i in range(0, fake_length):
        actual_fake_index = i + 1

        for j in range(0, 20):
            current_AA = list_amino_acid[j]
            freq = 0

            for k in range(0, count_homologs):
                current_seq = list_actual_aligned_sequence[k]

                if current_seq[i] == current_AA:
                    freq += 1

            residue_ID = str(actual_fake_index) + '_' + current_AA
            dic_fakeindexRes_count[residue_ID] = freq

    # print(dic_fakeindexRes_count, count_homologs)

    # initialize the 2-D array for observational
    Obs = numpy.full((20, fake_length), 0)
    # assigned the observational Site-specific count for each amino acid
    for i in range(0, fake_length):
        for j in range(0, 20):
            amino_type = dic_index_amino[j]
            site_Amino = str(i + 1) + '_' + amino_type
            count_site_amino = dic_fakeindexRes_count[site_Amino]

            Obs[j][i] = count_site_amino

    # pseu = numpy.full((fake_length, 1), 1)
    # print(Obs.shape)
    # print(pseu.shape)
    # N = numpy.matmul(Obs,pseu)
    # print(N)

    # print(Obs.shape)

    # The 1-D vector for prob PI
    # count each amino acid existing in MSA
    total_residue_count = 0
    count_row = numpy.full((20, 1), 0)
    for i in range(0, 20):
        for j in range(0, fake_length):
            count_row[i][0] += Obs[i][j]
            total_residue_count += Obs[i][j]
    Pi_row = count_row / total_residue_count

    print(Pi_row)

    return Obs, Pi_row


# Q = count_20AA_MSA(aligned_MSA_file = 'P00738.fasta')
# Numerical_eQt(givenQ = Q, time_interval = 20)

Obs_matrix, steady_Pi = Obtain_MSA_2dArray(aligned_fasta_file='P00738.fasta')


##########################################
#### Likelihood function #################
##########################################


####Calculate Last Layer EDGE (internal node + species node) + One Site
#### Once give the Q, time/branch, specices Residue, Pi_matrix
def PseudoProb_oneSite_OneLastEdge(rateQ, time_branchlength,
                                   species_sequence, steady_state_Pi):

    for m in range(0, len(species_sequence)):
        joint_prob = 1

        species_resI = species_sequence [m]
        if species_resI in list_amino_acid:
            # indicate which element in Matrix Are FOCUSING
            index_resI = dic_amino_index[species_resI]

            # general matrix P with time t
            Pt = Numerical_eQt(givenQ=rateQ, time_interval=time_branchlength)
            total_Prob = 0

            for j in range(0,20):
                p_ij_t = steady_state_Pi[j][0] * Pt[index_resI][j]
                # print(Pt[index_resI][j], p_ij_t)
                total_Prob += p_ij_t
            # print(total_Prob)
            ## total prob for site-specific is DONE

            joint_prob = joint_prob * total_Prob


    return 0

#### Given



PseudoProb_oneSite_OneLastEdge( rateQ = Q,
                                time_branchlength= 0.0,
                                species_resI = 'E',
                                steady_state_Pi= steady_Pi)
