
# list for Amino Acid One Letter
list_amino_acid = [
    "A",
    "R",
    "N",
    "D",
    "C",
    "Q",
    "E",
    "G",
    "H",
    "I",
    "L",
    "K",
    "M",
    "F",
    "P",
    "S",
    "T",
    "W",
    "Y",
    "V"
]

## Hash table for amino acid => list of codon
dic_aminoacid_codon = {
    'A': ['GCT', 'GCC', 'GCA', 'GCG'],
    'R': ['CGT', 'CGC', 'CGA', 'CGG', 'AGA', 'AGG'],
    'N': ['AAT', 'AAC'],
    'D': ['GAT', 'GAC'],
    'C': ['TGT', 'TGC'],
    'Q': ['CAA', 'CAG'],
    'E': ['GAA', 'GAG'],
    'G': ['GGT', 'GGC', 'GGA', 'GGG'],
    'H': ['CAT', 'CAC'],
    'I': ['ATT', 'ATC', 'ATA'],
    'L': ['CTT', 'CTC', 'CTA', 'CTG', 'TTA', 'TTG'],
    'K': ['AAA', 'AAG'],
    'M': ['ATG'],
    'F': ['TTT', 'TTC'],
    'P': ['CCT', 'CCC', 'CCA', 'CCG'],
    'S': ['TCT', 'TCC', 'TCA', 'TCG', 'AGT', 'AGC'],
    'T': ['ACT', 'ACC', 'ACA', 'ACG'],
    'W': ['TGG'],
    'Y': ['TAT', 'TAC'],
    'V': ['GTT', 'GTC', 'GTA', 'GTG']
}


# list_codon = []
# for k,v in dic_aminoacid_codon.items():
#     for e in v:
#         if e not in list_codon:
#             list_codon.append(e)
# print(len(list_codon))

def condon_distance(codon1, codon2):
    pair_codon_dist = 0

    for i in range(0,3):
        if codon1 [i] != codon2[i]:
            pair_codon_dist += 1
    # print(pair_codon_dist)
    return pair_codon_dist


def Check_aminoacid_connectivity(amino1, amino2):

    list_codon1_amino1 = dic_aminoacid_codon [amino1]
    list_codon2_amino2 = dic_aminoacid_codon [amino2]
    
    count_connected_codons = 0 

    for each_codon_in_one in list_codon1_amino1:
        for each_codon_in_two in list_codon2_amino2:
            tmp_seq_distance = condon_distance(each_codon_in_one, 
                                               each_codon_in_two)
            # print(each_codon_in_one, each_codon_in_two ,tmp_seq_distance)
            if tmp_seq_distance == 1:
                count_connected_codons += 1
    # print(count_connected_codons)

    return count_connected_codons

## assume that only allowing ONE nucleotide change of codon
list_unconnected_pairs = [] 
for i in range(0,20):
    for j in range(0, 20):
        type_1 = list_amino_acid[i]
        type_2 = list_amino_acid[j]
        number_connected_codons = \
               Check_aminoacid_connectivity(amino1=type_1, amino2=type_2)
        if number_connected_codons == 0:
            pair = type_1 + type_2 
            list_unconnected_pairs.append(pair)
# will collect 232 pair unconnected
print(list_unconnected_pairs)

