import re 
import os 

def Hash_SeqID_SpeciesID (ref_proteome_fasta_PATH):
    dic_SeqID_SpeciesID = {}
    
    f = open(ref_proteome_fasta_PATH, 'r')
    for line in f:
        if line.startswith('>'):
            seqID = re.split("\|", line)[1].upper().strip()
            try:
                nested_info = re.split("\|", line)[-1].upper().strip()
                nested_slices = re.split("\s", nested_info)
                for each_piece in nested_slices:
                    if each_piece.startswith('OX='):
                        speciesID = re.split("=", each_piece)[1].upper().strip()
            
                dic_SeqID_SpeciesID[seqID] = speciesID
            except:
                out_entry = ref_proteome_fasta_PATH + ',' + seqID + "\n"
                out_F = open('/home/bwang/seq_Species.log', 'a')
                out_F.write(out_entry)
                out_F.close()
    f.close()

    return dic_SeqID_SpeciesID

# raw_uniprot_refseq_folder = '/data/database/uniprot_Ref_Jun2019/OtherEukaryota/rawinfo'
# for root, subdir_list, file_list in os.walk(raw_uniprot_refseq_folder):
#     os.chdir(root)
#     for each_file in file_list:
#         if each_file.endswith('fasta'):
#             hash_seq_species = Hash_SeqID_SpeciesID(ref_proteome_fasta_PATH = each_file)
#             for k,v in hash_seq_species.items():
#                 out_entry = k + ',' + v 
#                 print(out_entry)

hash_seq_species = Hash_SeqID_SpeciesID(ref_proteome_fasta_PATH =\
                     '/data/database/uniprot_Ref_Jun2019/Human/rawsequence/UP000005640_9606.fasta')

out_F = open('/data/database/uniprot_Ref_Jun2019/OtherEukaryota/SeqID_SpeciesID', 'a')
for k,v in hash_seq_species.items():
    out_entry = k + ',' + v + "\n"
    out_F.write(out_entry)
out_F.close()