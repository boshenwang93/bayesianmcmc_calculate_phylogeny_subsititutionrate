import re 
import os 

def ObtainMSASpecies(MSA_file_PATH):
    dic_refSeqID_listspecies = {}

    refseqID = re.split('/', MSA_file_PATH)[-1]
    refseqID = re.split("\.", refseqID)[0].upper()

    list_species = [] 

    f = open(MSA_file_PATH, 'r')
    for line in f:
        if line.startswith('>'):
            seq_header_lastPC = re.split("\|", line)[2].upper().strip()
            species_name = re.split("_", seq_header_lastPC)[-1]
            if species_name not in list_species:
                list_species.append(species_name)
    f.close()
    
    dic_refSeqID_listspecies[refseqID] = list_species

    return dic_refSeqID_listspecies


MSA_folder = '/data/database/uniprot_Ref_Jun2019/homologs/orthologs/MSA/iden50/aligned/'
dic_All = {} 
list_unique_species = []

for root, subdir_list, file_list in os.walk(MSA_folder):
    for each_file in file_list:
        os.chdir(MSA_folder)
        dic_refSeqID_listspecies = ObtainMSASpecies(MSA_file_PATH = each_file)
        dic_All.update(dic_refSeqID_listspecies)

for refseqID, list_species in dic_All.items():
    for each_species in list_species:
        if each_species not in list_unique_species:
            list_unique_species.append(each_species)

list_high_similar_species = [] 
for each_species in list_unique_species:
    count = 0 
    for k,v in dic_All.items():
        if each_species in v:
            count += 1
    if count >= 3100:
        list_high_similar_species.append(each_species)

# print(len(list_high_similar_species))

for k,v in dic_All.items():
    species_overlap_count = 0
    for each_species in v:
        if each_species in list_high_similar_species:
            species_overlap_count += 1 
    if species_overlap_count == 25:
        print(k)